jQuery(document).ready(function ($) {
    
});


function add_item(btn)
{
    $('.modal').on('click', btn, function (e) {
        e.stopPropagation()
        var $this = $(this)

        var view = $this.data('view') ? $this.data('view') : '/main/ajax'
        var box = $this.data('box') ? $this.data('box') : '.zone-clone'
        var action = $this.data('action') ? $this.data('action') : 'box_ajax'
        var action = '/evaluate/' + action + '/' + view

        $(box).append($('<div class="block-item">').load(action, function (response, status, xhr) {
            if (status != 'success')
                alert('error al obtener los datos')
        }))
    })

}

/*
@autor: Jean Canevello
*/
$.fn.extend({
    resetSession: function () {
        var sto = sessionStorage.getItem('exc')

        if (sto)
            sessionStorage.removeItem('exc')
    }
})