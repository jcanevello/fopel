const { app, BrowserWindow, Menu } = require('electron')
const path = require('path')


if(process.env.NODE_ENV !== 'production') {
  require('electron-reload')(__dirname, {
    electron: path.join(__dirname, '../node_modules', '.bin', 'electron')
  });
}

function createWindow () {
  const win = new BrowserWindow({
    width: 1100,
    height: 600,
    //frame:false,
    //backgroundColor: '#2e2c29',
    resizable: false,
    webPreferences: {
      preload: path.join(__dirname, '../preload.js')
    }
  })

  //
  
  
  //Menu.setApplicationMenu(null)
  win.loadFile('src/html/index.html')

  win.webContents.openDevTools()
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
