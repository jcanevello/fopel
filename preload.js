window.addEventListener('DOMContentLoaded', () => {
  /*const replaceText = (selector, text) => {
    const element = document.getElementById(selector)
    if (element) element.innerText = text
  }
 
  for (const type of ['chrome', 'node', 'electron']) {
    replaceText(`${type}-version`, process.versions[type])
  }*/

  const fs = require('fs')
  const path = require('path')
  const parse = require('csv-parse')
  const $ = require('jquery')
  const data = []

  //const app = document.getElementById("app")

  function generateJsonContent() {
    fs.createReadStream(path.join(__dirname, 'src/data.csv'))
      .pipe(parse({ delimiter: '|', columns: true }))
      .on("data", (row) => data.push(row))
      .on("end", () => {
        data.forEach(function (r, i) {
          if (r.parent.length > 0) {
            if (Array.isArray(data[r.parent].menu))
              data[r.parent].menu.push(r.id)
            else
              data[r.parent].menu = [r.id]
          }
        })
        console.log(data)
      })

  }

  function loadHTML(parent, html) {
    const content = fs.readFileSync(path.join(__dirname, 'src/html', html + '.html'), 'utf8')
    const element = document.getElementById(parent)
    element.innerHTML = content;
  }

  function textToDomHtml(text) {
    return document.createRange().createContextualFragment(text)
  }

  function generateMenu(d) {
    let buttons = ''
    d.forEach(e => {
      buttons += '<button type="button" class="btn btn-primary btn-block btn-cap" data-cap="' + data[e].id + '">' + data[e].titulo + '</button>';
    });

    return buttons
  }

  function generateNav(capitulo) {
    let buttons = []

    buttons.push('<li class="active"><span>' + capitulo.titulo + '</span></li>')

    while (capitulo.parent.length > 0) {
      capitulo = data[capitulo.parent]
      buttons.push('<li><a class="btn-cap" data-cap="' + capitulo.id + '" href="#">' + capitulo.titulo + '</a></li>')
    }

    buttons.push('<li><a class="" id="btn-home" href="#">Inicio</a></li>')
    buttons.reverse()

    return buttons
  }

  function generateContentBody(capitulo) {
    let result = []
    let content_json = null
    let c = null

    if (capitulo.contenido.length > 0) {
      content_json = capitulo.contenido.replaceAll("'", '"')
      c = JSON.parse(content_json)

      for (let i in c) {
        result += generateObjecthtml(i, c[i])
      }
    }

    return result
  }

  function activateBtnMenu(capitulo) {
    let btn_menu = null
    
    $('#menu .btn').removeClass('active')
    btn_menu = $('#menu .btn[data-cap="' + capitulo.id + '"]')
    btn_menu.addClass('active')
    console.log(btn_menu.offset().top)
    if (btn_menu.offset().top > 500) {
      $('#app #menu').animate({
        scrollTop: $('#menu .btn[data-cap="' + capitulo.id + '"]').offset().top - 50
      }, 0);
    }
  }

  function getDataFromJson(capitulo_id) {
    let capitulo = data[capitulo_id]
    const template_content_html = textToDomHtml(fs.readFileSync(path.join(__dirname, 'src/html/template_content.html'), 'utf8'))
    const element = document.getElementById('app')
    let div_menu = document.getElementById('menu')

    //console.log(capitulo)
    if (capitulo.menu.length > 0) {
      //menu
      template_content_html.firstChild.children[0].children[0].innerHTML = generateMenu(capitulo.menu)
    }
    else {
      //menu
      if (div_menu != null) {
        template_content_html.firstChild.children[0].children[0].innerHTML = div_menu.innerHTML
      }
    }

    //Si se va a mostrar un el primer subtitulo, a la variable capitulo se le asigna el hijo indicado en data.csv
    if (capitulo.child.length > 0)
      capitulo = data[capitulo.child]

    //nav
    template_content_html.firstChild.children[1].children[0].innerHTML = generateNav(capitulo)

    //content-title
    template_content_html.firstChild.children[1].children[1].children[0].innerHTML = capitulo.titulo

    //content-body
    template_content_html.firstChild.children[1].children[1].children[1].innerHTML = generateContentBody(capitulo)

    element.innerHTML = ''
    element.appendChild(template_content_html)

    const e_titulo = document.getElementById('title')

    if (e_titulo != null) {
      e_titulo.innerHTML = capitulo.titulo
    }

    //activamos el botón seleccionado en el menú
    activateBtnMenu(capitulo)
  }

  function generateObjecthtml(tipo, valor) {
    let r = ''

    switch (tipo) {
      case 'video':
        r += generateObjectVideo(valor)
        break
      case 'imagen':
        r += generateObjectImagen(valor)
        break
      case 'template':
        r += generateObjectTemplate(valor)
      default:
    }

    return r
  }

  function generateObjectTemplate(name_html) {
    return fs.readFileSync(path.join(__dirname, 'src/html/content_body/' + name_html), 'utf8')
  }

  function generateObjectImagen(url) {
    return '<img src="../media/imagenes/' + url + '" class="img-fluid" alt="...">'
  }

  function generateObjectVideo(url) {
    return '<video id="video" width="100%" height="350" controls><source src="../media/videos/' + url + '"></video>'
  }
  /*
    $('body').on('click', '.content-card', function(e){
      //e.stopPropagation()
      let $this = $(this)
      let modal = $this.data('modal')
      console.log('#'+modal)
  
      console.log($('#exampleModalCenter'))
      console.log($('#exampleModalCenter')[0])
  
    })*/

  //loadHTML("app", "welcome");
  $(".progress-bar").animate({
      width: "100%"
  }, 7000, function(){
    $('#enter').removeClass('display-none')
    $('.bienvenido h1').css('margin-bottom', '-30px')
  });
  //$(".progress-bar").remove()

  loadHTML("app", "home");
  //loadHTML("app", "concepto");

  $('body').on('click', '#enter', function () {
    loadHTML("app", "home")
  })

  $('body').on('click', '#btn-home', function () {
    loadHTML("app", "home")
  })

  $('body').on('click', '.btn-cap', function () {
    //loadHTML("app", $(this).data('cap'))
    getDataFromJson($(this).data('cap'))
  })

  generateJsonContent()

})
